# Hello World!

.gitignore文件通过设置缺省文件的文件名，来忽略上传时候的要求

## linux 命令

* touch aaa.txt 创建一个aaa.txt文件
* mkdir test 创建一个test文件
* rm -rf  aaa.txt 删除aaa.txt文件

### git 常用命令

* git init 初始化一个仓库

* git add . 将所有的修改操作加载到暂存区

* git commit -m '在注释内容' 将暂存区的内容提交到本地仓库

* git log [option]查看日志
  * -all：显示所有分支
  * --pretty=oneline：将提交信息显示为一行
  * --abbrev-commit：使得输出的commitId更简短
  * --graph：以图的形式显示
  
* git status 查看修改的状态（暂存区）

* git branch 查看本地分支

* git branch 分支名 创建本地分支

* git checkout 分支名  切换分支

* git checkout -b 分支名 切换不存在的分支时会自动创建

* git merge 分支名称 合并分支

* git branch -d 分支名 删除分支

* git mv file_name new_file_name 重命名文件名称

  

```markdown
#用于输出git提交日志
alias git-log=' git log --pretty=oneline --abbrev-commit --graph --all'
#用于输出当前目录所有文件及基本信息
alias ll='ls -al'
```



```git
git init 
git add .
git commit -m 'aaa'
git-log (已经配置好了的)
git branch dev01
git checkout dev01
git add .
git commit -m 'abc'
git checkout master
git-log
git merge dev01
git status
git-log
git ..............



git reset --hard <commitID>
HEAD is now at 06f0966 rename redead.md
```

### 合并快速模式

```git
git checkout master 
git merge dev
```

![image-20220126113516415](images/image-20220126113516415.png)

![image-20220126120526155](images/image-20220126120526155.png)

在c:/user/用户名下操作



git remote add origin https://gitee.com/jiexi_root/git_test.git

git remote 查看remote分支

git push origin master 将本地master推流到gitee上

![image-20220126131720490](images/image-20220126131720490.png)

git push --set-upstream origin master 等同于 git push -u origin master

git push 可以省略后面的分支

git branch -vv 查看本地与远程仓库的关联

![image-20220126134903239](images/image-20220126134903239.png)

### git clone 克隆仓库

git clone https://gitee.com/jiexi_root/git_test.git test2

test2 为指定的文件名

![image-20220126191811035](images/image-20220126191811035.png)

### 解决远程合并分支问题

`git fetch`是将远程主机的最新内容拉到本地，用户在检查了以后决定是否合并到工作本机分支中。

而`git pull` 则是将远程主机的最新内容拉下来后直接合并，即：`git pull = git fetch + git merge`，这样可能会产生冲突，需要手动解决。

![image-20220207120032918](images/image-20220207120032918.png)

![image-20220207120959412](images/image-20220207120959412.png)

![image-20220207121155515](images/image-20220207121155515.png)

删除remote远程分支：` git remote remove origin` 删除即可

> `git push -u`与`git push --set-upstream`区别

![image-20220207123002110](images/image-20220207123002110.png)

#### 远程仓库练习

```shell
#git 远程仓库练习
仓库名：git_test_clone

mkdir git_test_clone
cd git_test_clone
git init 
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin git@gitee.com:jiexi_root/git_test_clone.git
git push -u origin "master" 

去网站更改README.md内容
vi file03.txt
git pull 拉取远程操作
git add .
git commit -m 'add file03'
git push
```

![image-20220207123812355](images/image-20220207123812355.png)

### 配置ssh连接

#### 生成

1.配置

```shell
git config –global user.name "xxxxx"
git config –global user.email "xxx@xx.xxx"

注：查下是否配置成功，用命令
git config --global --list

```

![image-20220223105519856](images/image-20220223105519856.png)

2.生成秘钥

```shell
ssh-keygen -t rsa -C  "上面的邮箱"
注：执行上面命令后，连续回车3次
```

#### 打印

1、进入ssh

```shell
cd ~/.ssh
```

2、打印ssh

```shell
cat id_rsa.pub
```
